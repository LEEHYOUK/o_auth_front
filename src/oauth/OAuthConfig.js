import qs from 'qs';

export const OAUTHSERVERADDRESS = "http://183.111.176.8:8088/oauth";

export const OAUTHSERVERTOKENPATH = "/oauth/token";

export const GRANT_TYPE = {
	PASSWORD: "password",
	REFRESH: "refresh_token"
};

export const OAUTHCREDENTIALS = {
	auth: {
		username: "clientapp",
		password: "2018"
	}
};

export const dataEncoder = (data) => {
	return qs.stringify(data);
}
