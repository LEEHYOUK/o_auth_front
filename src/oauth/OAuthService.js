import * as config from './OAuthConfig';
import axios from 'axios';
import cookie from 'react-cookies';

export const OAuthLogin = (userId, userPassword, successFunc, errorFunc) => {
	axios.post(config.OAUTHSERVERADDRESS + config.OAUTHSERVERTOKENPATH, config.dataEncoder({username: userId, password: userPassword, grant_type: config.GRANT_TYPE.PASSWORD}), config.OAUTHCREDENTIALS).then((resp) => {
		let accessTokenData = JSON.parse(atob(resp.data.access_token.substring(resp.data.access_token.indexOf('.') + 1, resp.data.access_token.lastIndexOf('.'))));
		cookie.save("token", {
			access_token: resp.data.access_token,
			refresh_token: resp.data.refresh_token,
			access_expire: (accessTokenData.exp * 1000),
			user_no: accessTokenData.user_no,
			user_email: accessTokenData.user_name
		});
		console.log(new Date(cookie.load("token").access_expire));
		if (successFunc != null) {
			successFunc(resp.data);
		}
	}).catch((err) => {
		if (errorFunc != null) {
			errorFunc((err.response.data));
		}
	});
}

export const OAuthExpireCheck = () => {
	if (cookie.load("token").access_expire < new Date().getTime()) {
		axios.post(config.OAUTHSERVERADDRESS + config.OAUTHSERVERTOKENPATH, config.dataEncoder({refresh_token: cookie.load("token").refresh_token, grant_type: config.GRANT_TYPE.REFRESH}), config.OAUTHCREDENTIALS).then((resp) => {
			console.log(resp.data);
		}).catch((err) => {
			console.log(err.data);
		})
	} else {
        console.log("notexpire");
    }
}
