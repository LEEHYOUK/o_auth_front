import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import {Login, Join} from './container';

import Layout from './component/Layout';

class App extends React.PureComponent {
	render() {
		return (
			<BrowserRouter>
				<Layout>
					<Switch>
						<Route exact path={"/"} component={Login}/>
						<Route path={"/join"} component={Join}/>
					</Switch>
				</Layout>
			</BrowserRouter>
		);
	}
}

export default App;
