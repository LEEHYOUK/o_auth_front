import React from 'react';
import axios from 'axios';
import qs from 'qs';

class Join extends React.PureComponent {
    constructor(props) {
        super(props);
        this.onClickSubmit = this.onClickSubmit.bind(this);
    }

    onClickSubmit(){
        axios.post("http://183.111.176.8:8088/oauth"+"/user/join",qs.stringify({
            userId:this.userId.value,
            userPwd:this.userPwd.value,
            userName:this.userName.value,
            userMobile:this.userMobile.value,
            smsYn:this.smsYn.checked,
            emailYn:this.emailYn.checked
        })).then((resp)=>{
            console.log(resp);
        }).catch((err)=>{
            console.log(err.response);
        })
    }

    render () {
        return (
            <div>
                <div>
                    <label>아이디</label>
                    <input ref={(ref)=>{this.userId = ref}} placeholder={"아이디"}/>
                </div>
                <div>
                    <label>비밀번호</label>
                    <input ref={(ref)=>{this.userPwd = ref}} placeholder={"비밀번호"}/>
                </div>
                <div>
                    <label>이름</label>
                    <input ref={(ref)=>{this.userName = ref}} placeholder={"이름"}/>
                </div>
                <div>
                    <label>핸드폰</label>
                    <input ref={(ref)=>{this.userMobile = ref}} placeholder={"핸드폰"}/>
                </div>
                <div>
                    <label>sms 동의</label>
                    <input ref={(ref)=>{this.smsYn = ref}} type="checkbox" placeholder={"sms 동의"}/>
                </div>
                <div>
                    <label>email 동의</label>
                    <input ref={(ref)=>{this.emailYn = ref}} type="checkbox" placeholder={"email 동의"}/>
                </div>
                <div>
                    <button onClick={this.onClickSubmit}>회원가입</button>
                </div>
            </div>
        )
    }
}

export default Join;
