import React from 'react';
import {OAuthLogin} from '../oauth/OAuthService';
import cookie from 'react-cookies';

class Login extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			login: false,
			errorMsg: ""
		}
		this.onClickSubmit = this.onClickSubmit.bind(this);
	}

	onClickSubmit() {
		OAuthLogin(this.username.value, this.password.value, (data) => {
			this.setState({login: true});
		}, (err) => {
			this.setState({
				login: false,
				errorMsg: err.error_description
			}, () => {
				setTimeout(() => {
					this.setState({errorMsg: ""})
				}, 5000);
			});
		});
	}

	render() {
		const loginForm = (
			<div>
				<input ref={(ref) => {
					this.username = ref
				}} placeholder={"Email"}/>
				<input ref={(ref) => {
					this.password = ref
				}} placeholder={"Password"}/>
				<button onClick={this.onClickSubmit}>로그인</button>
				{this.state.errorMsg}
			</div>
		);
		const logined = (
			<div>
				<div>
					로그인 성공
				</div>
			</div>
		);

		return (
			<div>
				{((!this.state.login)
					? loginForm
					: logined)}
			</div>
		)
	}
}

export default Login;
