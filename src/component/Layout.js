import React from 'react';
import Menu from './Menu';

const Layout = (props) => {
	return (
        <div>
            <div>
                <Menu/>
            </div>
            <div>
                {props.children}
            </div>
        </div>
	)
}

export default Layout;
