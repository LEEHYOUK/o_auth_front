import React from 'react'
import { Link } from 'react-router-dom';

const Menu = (props) => {
	return (
		<div>
            <Link to={"/"}>로그인폼</Link>
            <br/>
            <Link to={"/join"}>회원가입폼</Link>
        </div>
	)
}

export default Menu
