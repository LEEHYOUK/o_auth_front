var webpack = require('webpack');

module.exports = {
	entry: './src/index.js',

	output: {
		path: __dirname + '/public',
		filename: 'bundle.js'
	},

	devServer: {
		historyApiFallback: true,
		inline: true,
		port: 80,
		contentBase: __dirname + '/public'
	},

	module: {
    rules: [
			{
				test: /\.css$/,
				use: [
					{
						loader: 'style-loader'
					}, {
						loader: 'css-loader'
					}, {
						loader: 'postcss-loader'
					}
				]
			}, {
				test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
            presets: ['env', 'stage-0', 'react']
        }
			}, {
				test: /\.scss$/,
				exclude: /node_modules/,
                loaders: [
					'style-loader', 'css-loader', 'sass-loader'
				],
			}
		]
	}
};
